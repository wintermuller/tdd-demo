package hu.dpc.edu.books.entity;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static hu.dpc.edu.books.entity.Predicates.contains;
import static hu.dpc.edu.books.entity.Predicates.equalTo;

public class InMemoryBookRepository {

    private Map<Long, Book> bookById;
    private IdGenerator idGenerator;

    public InMemoryBookRepository(Map<Long, Book> bookById, IdGenerator idGenerator) {
        this.bookById = bookById;
        this.idGenerator = idGenerator;
    }

    public long add(Book book) {
        Objects.requireNonNull(book, "book should not be null");
        if (book.getTitle() == null) {
            throw new IllegalArgumentException("Book title is required");
        }

        final long id = idGenerator.generateId();

        final Book managedBook = new Book(book);

        managedBook.setId(id);

        bookById.put(id, managedBook);

        return id;
    }

    public Book findById(long id) {
        final Book managedBook = findReferenceById(id);
        return new Book(managedBook);
    }

    private Book findReferenceById(long id) {
        final Book managedBook = bookById.get(id);
        if (managedBook == null) {
            throw new EntityNotFoundException();
        }
        return managedBook;
    }

    public List<Book> findAll() {
        return bookById.values().stream()
                .map(book -> {
                    return new Book(book);
                })
                .collect(Collectors.toList());
    }

    public void update(Book book) {
        Objects.requireNonNull(book, "book should not be null");

        if (book.getId() == null) {
            throw new EntityNotFoundException();
        }

        if (book.getTitle() == null || book.getTitle().isEmpty()) {
            throw new IllegalArgumentException();
        }

        if (book.getAuthor() == null || book.getAuthor().isEmpty()) {
            throw new IllegalArgumentException();
        }

        Book managedBook = findReferenceById(book.getId());
        managedBook.setTitle(book.getTitle());
        managedBook.setAuthor(book.getAuthor());
        bookById.put(book.getId(), managedBook);
    }

    public List<Book> findAllByExample(Book example) {
        Stream<Book> bookStream = bookById.values().stream();

        final Stream<Book> filteredBookStream =
                new FilteredStreamBuilder<>(bookStream)
                        .propertyContains(Book::getTitle, example.getTitle())
                        .propertyContains(Book::getAuthor, example.getAuthor())
                        .propertyEqualsTo(Book::getId, example.getId())
                        .build();

        return filteredBookStream
                .map(Book::new)
                .collect(Collectors.toList());

    }
}
